import time

from selenium import webdriver
from selenium.webdriver.common.by import By
class TestCESHIREN():
    def setup_method(self, method):
        self.driver = webdriver.Chrome()
        self.vars = {}

    def teardown_method(self, method):
        self.driver.quit()

    # 主代码
    def test_cESHIREN(self):
        self.driver.get("https://ceshiren.com/")
        time.sleep(4)
        self.driver.set_window_size(1060, 816)
        #定位搜索按钮
        self.driver.find_element(By.CSS_SELECTOR, ".d-icon-search").click()
        self.driver.find_element(By.ID, "search-term").click()
        #输入关键词
        self.driver.find_element(By.ID, "search-term").send_keys("赵含雪")
        time.sleep(4)
        #点击按钮操作
        self.driver.find_element(By.CSS_SELECTOR, ".item:nth-child(1) .user-result").click()
